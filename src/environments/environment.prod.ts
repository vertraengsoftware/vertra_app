export const environment = {
  production: true,
  hereMaps: {
    appId: '${PROD_HEREMAPS_APPID}',
    appCode: '${PROD_HEREMAPS_APPCODE}'
  },
  googleMaps: {
    apiKey: '${PROD_GOOGLEMAPS_APIKEY}'
  },
  apiPath: 'https://vertra-json-server.firebaseapp.com/api/',
  firebase: {
    apiKey: '${PROD_FIREBASE_APIKEY}',
    projectId: '${PROD_FIREBASE_PROJECTID}'
  }
};
