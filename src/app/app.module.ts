// angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { environment } from 'src/environments/environment';

// material-design
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatCardModule,
  MatButtonToggleModule,
  MatButtonModule,
  MatToolbarModule,
  MatSelectModule,
  MatIconModule,
  MatInputModule,
  MatTooltipModule,
  MatRadioModule,
  MatSliderModule,
  MatCheckboxModule,
  MatListModule,
  MatDividerModule
} from '@angular/material';

// ng-bootstrap
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';

// map-box
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

// infinite-scroll
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// ngx-gallery/gallerize
import { GalleryModule } from '@ngx-gallery/core';
import { LightboxModule } from '@ngx-gallery/lightbox';
import { GallerizeModule } from '@ngx-gallery/gallerize';

// @agm/core
import { AgmCoreModule } from '@agm/core';

// ng5-slider
import { Ng5SliderModule } from 'ng5-slider';

// app-components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './components/maps/map/map.component';
import { SearchContainerComponent } from './containers/search-container/search-container.component';
import { AdCardComponent } from './components/ad-card/ad-card.component';
import { HeaderComponent } from './components/header/header.component';
import { AdCardToolbarComponent } from './components/ad-card-toolbar/ad-card-toolbar.component';
import { MapLayerComponent } from './components/maps/map-layer/map-layer.component';
import { MapAdLayerComponent } from './components/maps/map-ad-layer/map-ad-layer.component';
import { MapAdPopupComponent } from './components/maps/map-ad-popup/map-ad-popup.component';
import { PhotoCarouselComponent } from './components/photo-carousel/photo-carousel.component';
import { FormsModule } from '@angular/forms';
import { AdSearchComponent } from './components/ad-search/ad-search.component';
import { MapStorePopupComponent } from './components/maps/map-store-popup/map-store-popup.component';
import { AdDetailsContainerComponent } from './containers/ad-details-container/ad-details-container.component';
import { PhotoGalleryComponent } from './components/photo-gallery/photo-gallery.component';
import { AdSearchFilterComponent } from './components/ad-search-filter/ad-search-filter.component';
import { StreetViewComponent } from './components/street-view/street-view.component';
import { AdContactComponent } from './components/ad-contact/ad-contact.component';
import { registerLocaleData } from '@angular/common';

import localeBr from '@angular/common/locales/br';
import { AdContentFeaturesComponent } from './components/ad-content/ad-content-features/ad-content-features.component';
import { AdContentAboutComponent } from './components/ad-content/ad-content-about/ad-content-about.component';
import { AdContentFinanceComponent } from './components/ad-content/ad-content-finance/ad-content-finance.component';
import { AdContentLegalComponent } from './components/ad-content/ad-content-legal/ad-content-legal.component';
import { AdContentWhyComponent } from './components/ad-content/ad-content-why/ad-content-why.component';
import { LandingPageContainerComponent } from './containers/landing-page-container/landing-page-container.component';
import { AgentCardComponent } from './components/agent-card/agent-card.component';

registerLocaleData(localeBr, 'br');

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    SearchContainerComponent,
    AdCardComponent,
    HeaderComponent,
    AdCardToolbarComponent,
    MapLayerComponent,
    MapAdLayerComponent,
    MapAdPopupComponent,
    PhotoCarouselComponent,
    AdSearchComponent,
    MapStorePopupComponent,
    AdDetailsContainerComponent,
    PhotoGalleryComponent,
    AdSearchFilterComponent,
    StreetViewComponent,
    AdContactComponent,
    AdContentFeaturesComponent,
    AdContentAboutComponent,
    AdContentFinanceComponent,
    AdContentLegalComponent,
    AdContentWhyComponent,
    LandingPageContainerComponent,
    AgentCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoiZ3RvcnJlc20iLCJhIjoiY2p3Yms2aWxjMGxuYjRhbGp5cnNxNnQ4diJ9.7Eebe2fqdsMfhcqsw5XAdw'
    }),
    BrowserAnimationsModule,
    MatButtonToggleModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatSelectModule,
    MatIconModule,
    MatRadioModule,
    MatSliderModule,
    MatCheckboxModule,
    MatDividerModule,
    MatListModule,
    NgbCarouselModule,
    MatInputModule,
    FormsModule,
    InfiniteScrollModule,
    MatTooltipModule,
    GalleryModule,
    LightboxModule,
    GallerizeModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMaps.apiKey
    }),
    Ng5SliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
