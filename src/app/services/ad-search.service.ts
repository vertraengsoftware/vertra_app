import { AdContactType } from './../interfaces/ad/ad-contact-type.enum';
import { AdFilter } from './../interfaces/ad/ad-filter';
import { Injectable } from '@angular/core';
import { AdCard } from '../interfaces/ad/ad-card';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdSearchService {
  adCards$: BehaviorSubject<AdCard[]>;
  withUnboundedAds: AdCard[];
  adCardsMap$: BehaviorSubject<AdCard[]>;
  withUnboundedAdsMap: AdCard[];
  adFilter$: BehaviorSubject<AdFilter>;

  constructor(private httpClient: HttpClient) {
    this.adCards$ = new BehaviorSubject<AdCard[]>([]);
    this.adCardsMap$ = new BehaviorSubject<AdCard[]>([]);
    this.adFilter$ = new BehaviorSubject<AdFilter>(null);
  }

  filter(filter: AdFilter): void {
    this.httpClient.get<AdCard[]>(environment.apiPath + (filter ? 'adCards?_limit=2' : 'adCards?_limit=4')).subscribe(adCards => {
      this.updateAdCards(adCards);
      this.adFilter$.next(filter);
    });
  } //TODO pesquisa filtro no backend

  filterMap(filter: AdFilter): void {
    this.httpClient.get<AdCard[]>(environment.apiPath + (filter ? 'adCards?_limit=2' : 'adCards?_limit=4')).subscribe(adCards => {
      this.updateAdCardsMap(adCards);
      this.adFilter$.next(filter);
    });
  } //TODO pesquisa filtro mapa no backend

  searchAd(word: string): void {
    this.httpClient.get<AdCard[]>(environment.apiPath + 'adCards?_limit=2').subscribe(adCards => {
      this.updateAdCards(adCards);
    });
  } //TODO pesquisa no backend

  getFirstAds(): void {
    this.httpClient.get<AdCard[]>(environment.apiPath + 'adCards?_limit=4').subscribe(adCards => {
      this.updateAdCards(adCards);
    });
  }

  getMapAds(): void {
    this.httpClient.get<AdCard[]>(environment.apiPath + 'adCards?').subscribe(adCards => {
      this.updateAdCardsMap(adCards);
    });
  }

  filterQuantity(quantity: number): void {
    this.httpClient.get<AdCard[]>(environment.apiPath + 'adCards?_limit=' + quantity).subscribe(adCards => {
      this.updateAdCards(adCards);
    });
  }

  filterByBounds(north: number, south: number, east: number, west: number) {
    this.adCards$.next(this.withUnboundedAds.filter(x => {
      const lat = x.property.coordinates.latitude;
      const lgn = x.property.coordinates.longitude;
      return lgn >= west && lgn <= east && lat >= south && lat <= north;
    }));
  }

  getNextPage(adCount: number) {
    this.httpClient.get<AdCard[]>(environment.apiPath + 'adCards?_limit=' + (adCount + 2)).subscribe(adCards => {
      this.updateAdCards(adCards);
    });
  }

  updateAdCards(adCards: AdCard[]) {
    this.withUnboundedAds = adCards;
    this.adCards$.next(adCards);
  }

  updateAdCardsMap(adCards: AdCard[]) {
    this.withUnboundedAdsMap = adCards;
    this.adCardsMap$.next(adCards);
  }

  adCard2GeoJsonFeatureCollection(adCard: AdCard[]): GeoJSON.FeatureCollection<GeoJSON.Point> {
    const retorno: GeoJSON.FeatureCollection<GeoJSON.Point> = {
      type: 'FeatureCollection',
      features: []
    };

    adCard.map(x => {
      retorno.features.push({
        type: 'Feature',
        id: x.id,
        geometry: {
          type: 'Point',
          coordinates: [
            x.property.coordinates.longitude,
            x.property.coordinates.latitude
          ]
        },
        properties: x
      });
    });

    return retorno;
  }

  adCard2GeoJsonFeature(adCard: AdCard): GeoJSON.Feature<GeoJSON.Point> {
    return this.adCard2GeoJsonFeatureCollection([adCard]).features[0];
  }

}
