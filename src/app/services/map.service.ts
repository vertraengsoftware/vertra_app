import { Injectable } from '@angular/core';
import { Layer } from '../interfaces/map/layer';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  private filterLayers: Layer[] =
    [
      {
        id: 'valorizacao',
        label: 'Valorização',
        url: 'mapbox://gtorresm.135w29g1',
        sourceLayer: 'bairrosBH-37c960',
        type: 'fill',
        style: {
          'fill-opacity': 0.65,
          'fill-color': {
            property: 'PRECO_MEDIO',
            stops: [[0, 'hsl(137, 38%, 25%)'], [150, 'hsl(121, 78%, 65%)'], [300, 'hsl(124, 96%, 81%)'], [450,'hsl(59, 73%, 56%)'], [650, 'hsl(33, 88%, 62%)'], 
            [800, 'hsl(0, 93%, 79%)'], [1300, 'hsl(0, 94%, 44%)']]
            }
        },
        layout: {
          visibility: 'none'
        }
      },
      {
      id: 'transito',
      label: 'Trânsito',
      url: 'mapbox://mapbox.mapbox-traffic-v1',
      sourceLayer: 'traffic',
      type: 'line',
      style: {
      'line-color': { 
      type: 'categorical',
      property: 'congestion',
      stops:
      [
        [
          'low',
          'hsl(120, 78%, 48%)'
        ],
        [
          'moderate',
          'hsl(35, 100%, 50%)'
        ],
        [
          'heavy',
          'hsl(0, 100%, 65%)'
        ],
        [
          'severe',
          'hsl(340, 57%, 37%)'
        ]
      ],          
      default: 'hsla(0, 0%, 100%, 0)'
      }
      },
      layout: {
        visibility: 'none'
      }
      },
      {
        id: 'educacao',
        label: 'Educação',
        url: 'mapbox://gtorresm.81e2wx2t',
        sourceLayer: 'Escolas-709c89',
        type: 'circle',
        style: {
          'circle-radius': 3,
          'circle-color': '#d9bf2e'
        },
        layout: {
          visibility: 'none'
        }
      },
      {
        id: 'saude',
        label: 'Saúde',
        url: 'mapbox://gtorresm.dyd3oxlx',
        sourceLayer: 'Hospitais-bhbhsa',
        type: 'circle',
        style: {
          'circle-radius': 3,
          'circle-color': '#00aeef'
        },
        layout: {
          visibility: 'none'
        }
      },
      {
        id: 'restaurantes',
        label: 'Restaurantes',
        url: 'mapbox://gtorresm.1kozfikd',
        sourceLayer: 'RestaurantesLanchonetesBares-dn5q6q',
        type: 'circle',
        style: {
          'circle-radius': 2,
          'circle-color': '#754c24'
        },
        layout: {
          visibility: 'none'
        }
      },
      {
        id: 'padariasesupermercados',
        label: 'Supermercado',
        url: 'mapbox://gtorresm.cv6vhgf3',
        sourceLayer: 'SupermercadosEPadarias-375xf4',
        type: 'circle',
        style: {
          'circle-radius': 2,
          'circle-color': '#00a651'
        },
        layout: {
          visibility: 'none'
        }
      },
      {
        id: 'vidaativa',
        label: 'Vida Ativa',
        url: 'mapbox://gtorresm.ahc4asx9',
        sourceLayer: 'VidaAtiva-9xti6q',
        type: 'circle',
        style: {
          'circle-radius': 3,
          'circle-color': '#d92e98'
        },
        layout: {
          visibility: 'none'
        }
      },
      {
        id: 'inundacao',
        label: 'Perigos',
        url: 'mapbox://gtorresm.bh6v3n5t',
        sourceLayer: 'areas_de_inundacao-5wp07v',
        type: 'fill',
        style: {
          'fill-color': '#2061a7'
        },
        layout: {
          visibility: 'none'
        }
      }
    ];

  private cityLayers: Layer[] = [
    {
      id: 'belohorizonte',
      label: 'Belo Horizonte',
      url: 'mapbox://gtorresm.310pnx9r',
      sourceLayer: 'ContronoBH-8vgmm8',
      type: 'line',
      style: {
        'line-color': '#5E35F2',
        'line-width': 2
      },
      layout: {
        visibility: 'visible'
      }
    },
    {
      id: 'bairros',
      label: 'Bairros',
      url: 'mapbox://gtorresm.caa0czu3',
      sourceLayer: 'bairrosBH-dpm35r',
      type: 'line',
      style: {
        'line-color': '#000000',
        'line-width': 1,
        'line-opacity': 0.04
      },
      layout: {
        visibility: 'visible'
      }
    }
  ];

  constructor() { }

  public getDefaultLayers(): Layer[] {
    return this.filterLayers;
  }

  public getCityBorder(id: string): Layer {
    return this.cityLayers.filter(x => x.id === id)[0];
  }
}
