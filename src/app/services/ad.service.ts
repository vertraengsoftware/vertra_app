import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Ad } from '../interfaces/ad/ad';
import { AdContactType } from '../interfaces/ad/ad-contact-type.enum';

@Injectable({
  providedIn: 'root'
})
export class AdService {
  currentAd$: BehaviorSubject<Ad>;

  constructor(private httpClient: HttpClient) {
    this.currentAd$ = new BehaviorSubject<Ad>(null);
  }

  getDetails(id: string) {
    this.httpClient.get<Ad>(environment.apiPath + 'ad-details/' + id).subscribe(ad => {
      this.currentAd$.next(ad);
    });
  }

  saveContact(codAd: string, typeContat: AdContactType) {
    console.log('contato gravado.');
  }

  ad2GeoJsonFeatureCollection(ad: Ad): GeoJSON.FeatureCollection<GeoJSON.Point> {
    if (!ad) {
      return null;
    }

    const retorno: GeoJSON.FeatureCollection<GeoJSON.Point> = {
      type: 'FeatureCollection',
      features: []
    };

    retorno.features.push({
      type: 'Feature',
      id: ad.id,
      geometry: {
        type: 'Point',
        coordinates: [
          ad.property.coordinates.longitude,
          ad.property.coordinates.latitude
        ]
      },
      properties: ad
    });

    return retorno;
  }
}
