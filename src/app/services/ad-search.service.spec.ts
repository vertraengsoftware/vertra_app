import { TestBed } from '@angular/core/testing';

import { AdSearchService } from './ad-search.service';

describe('AdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdSearchService = TestBed.get(AdSearchService);
    expect(service).toBeTruthy();
  });
});
