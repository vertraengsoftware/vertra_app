import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LandingPageContainerComponent } from './containers/landing-page-container/landing-page-container.component';
import { SearchContainerComponent } from './containers/search-container/search-container.component';
import { AdDetailsContainerComponent } from './containers/ad-details-container/ad-details-container.component';

const routes: Routes = [
  { path: '', component: LandingPageContainerComponent },
  { path: 'busca', component: SearchContainerComponent },
  { path: 'anuncio/:id', component: AdDetailsContainerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
