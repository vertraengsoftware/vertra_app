import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-photo-carousel',
  templateUrl: './photo-carousel.component.html',
  styleUrls: ['./photo-carousel.component.sass']
})
export class PhotoCarouselComponent implements OnInit {
  @Input()
  photos: string[];

  @Input()
  showControls: boolean;

  constructor() { }

  ngOnInit() {
  }

}
