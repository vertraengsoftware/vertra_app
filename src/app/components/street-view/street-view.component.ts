import { Coordinates } from './../../interfaces/general/coordinates';
import { Component, OnInit, Input, ViewChild, Inject, PLATFORM_ID } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-street-view',
  templateUrl: './street-view.component.html',
  styleUrls: ['./street-view.component.sass']
})
export class StreetViewComponent implements OnInit {
  @ViewChild('streetviewMap', null) streetviewMap: any;
  @ViewChild('streetviewPano', null) streetviewPano: any;

  @Input() coordinates: Coordinates;
  @Input() zoom = 5;
  @Input() heading = 10;
  @Input() pitch = 4;
  @Input() scrollwheel = true;

  constructor(@Inject(PLATFORM_ID) private platformId: any, private mapsAPILoader: MapsAPILoader) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.mapsAPILoader.load().then(() => {
        const center = { lat: this.coordinates.latitude, lng: this.coordinates.longitude };
        const map = new window['google'].maps.Map(
          this.streetviewMap.nativeElement,
          { center, zoom: this.zoom, scrollwheel: this.scrollwheel });
        const panorama = new window['google'].maps.StreetViewPanorama(
          this.streetviewPano.nativeElement, {
            position: center,
            pov: { heading: this.heading, pitch: this.pitch },
            scrollwheel: this.scrollwheel
          });
        map.setStreetView(panorama);
      });
    }
  }

}
