import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AdContactType } from 'src/app/interfaces/ad/ad-contact-type.enum';
import { Ad } from 'src/app/interfaces/ad/ad';
import { MatIconRegistry } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import { Agent } from 'src/app/interfaces/agent/agent';

@Component({
  selector: 'app-ad-contact',
  templateUrl: './ad-contact.component.html',
  styleUrls: ['./ad-contact.component.sass']
})
export class AdContactComponent implements OnInit {
  displayModal: boolean;
  @Input() adDetail: Ad;
  @Input() agent: Agent;
  @Output() contactClick = new EventEmitter<{ id: string, type: AdContactType }>();

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
    this.matIconRegistry.addSvgIcon("whatsapp", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../assets/icons/whatsapp.svg"))
    this.displayModal = false;
  }

  ngOnInit() {
  }

  onDisplayPhoneClick(event: Event) {
    event.stopPropagation();
    this.displayModal = true;
    this.contactClick.emit({ id: this.adDetail.id, type: AdContactType.phone });
  }

  onWhatsAppClick(event: Event) {
    event.stopPropagation();
    window.open(this.getWhatsAppMessage('55'+ this.agent.contact.ddd + this.agent.contact.wpp), '_blank'); //TODO mudar quando operacional
    //window.open(this.getWhatsAppMessage('55'+'3188950925'), '_blank');
    this.contactClick.emit({ id: this.adDetail.id, type: AdContactType.whatsApp });
  }

  getWhatsAppMessage(phoneNumber: string): string {
    return 'https://api.whatsapp.com/send?phone=' + phoneNumber + '&text=Estou interessado no imóvel de código ' + this.adDetail.id + ' localizado no bairro ' 
    + this.adDetail.property.address.neighborhood + ' que vi na Vertra.';
  }
}
