import { AdFilter } from './../../interfaces/ad/ad-filter';
import { KeyValue } from './../../interfaces/general/key-value';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Checkbox } from 'src/app/interfaces/inputs/checkbox';
import { Options, LabelType } from 'ng5-slider';
import { Range } from 'src/app/interfaces/general/range';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'app-ad-search-filter',
  templateUrl: './ad-search-filter.component.html',
  styleUrls: ['./ad-search-filter.component.sass']
})
export class AdSearchFilterComponent implements OnInit {
  @Input() filter: AdFilter;
  @Output() closeClick = new EventEmitter();
  @Output() resetClick = new EventEmitter();
  @Output() applyClick = new EventEmitter<AdFilter>();

  propertyFeatureOptions: Checkbox[] = [
    { id: 'pentHouse', label: 'Apartamento cobertura', value: false },
    { id: 'furnished', label: 'Armário embutido', value: false },
    { id: 'privateSwimmingPool', label: 'Piscina privativa', value: false },
    { id: 'dce', label: 'Quarto de serviço', value: false }
  ];

  condominiumFeatureOptions: Checkbox[] = [
    { id: 'academy', label: 'Academia', value: false },
    { id: 'barbecue', label: 'Churrasqueira', value: false },
    { id: 'elevator', label: 'Elevador', value: false },
    { id: 'gourmetSpace', label: 'Espaço gourmet na área comum', value: false },
    { id: 'swimmmingPool', label: 'Piscina', value: false },
    { id: 'sauna', label: 'Sauna', value: false },
    { id: 'playground', label: 'Playground', value: false },
    { id: 'fullTimeReception', label: 'Portaria 24h', value: false },
    { id: 'sportsCourt', label: 'Quadra esportiva', value: false },
    { id: 'partyRoom', label: 'Salão de festas', value: false },
    { id: 'acceptsPet', label: 'Aceita pets', value: false }
  ];

  quantities: KeyValue<number, string>[] = [
    { key: 1, value: '1' },
    { key: 2, value: '2' },
    { key: 3, value: '3' },
    { key: 4, value: '4+' }
  ];

  priceOptions: Options = {
    floor: 200000,
    ceil: 2000000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return 'R$' + formatNumber(value, 'br');
        case LabelType.High:
          return 'R$' + formatNumber(value, 'br');
        default:
          return 'R$' + formatNumber(value, 'br');
      }
    },
    step: 5000
  };

  areaOptions: Options = {
    floor: 50,
    ceil: 1000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return formatNumber(value, 'br') + 'm²';
        case LabelType.High:
          return formatNumber(value, 'br') + 'm²';
        default:
          return formatNumber(value, 'br') + 'm²';
      }
    },
    step: 50
  };

  selectedPriceRange: Range = { floor: this.priceOptions.floor, ceil: this.priceOptions.ceil };
  selectedAreaRange: Range = { floor: this.areaOptions.floor, ceil: this.areaOptions.ceil };
  selectedRooms: number;
  selectedBathrooms: number;
  selectedParkingSpaces: number;

  ngOnInit() {
    if (this.filter) {
      this.setFilter();
    }
  }

  onCloseClick(event: Event): void {
    event.stopPropagation();
    this.closeClick.next();
  }

  onResetClick(event: Event): void {
    this.reset();
    event.stopPropagation();
    this.resetClick.next();
  }

  onApplyClick(event: Event): void {
    const adFilter: AdFilter = this.getAdFilter();
    event.stopPropagation();
    this.applyClick.emit(adFilter);
  }

  getAdFilter(): AdFilter {
    return {
      adType: null,
      priceRange: this.selectedPriceRange,
      propertyType: null,
      areaRange: this.selectedAreaRange,
      address: null,
      propertyFeatures: {
        bedrooms: this.selectedRooms,
        suites: null,
        bathrooms: this.selectedBathrooms,
        parkingSpaces: this.selectedParkingSpaces,
        floor: null,
        pentHouse: this.propertyFeatureOptions.filter(x => x.id === 'pentHouse')[0].value,
        furnished: this.propertyFeatureOptions.filter(x => x.id === 'furnished')[0].value,
        privateSwimmingPool: this.propertyFeatureOptions.filter(x => x.id === 'privateSwimmingPool')[0].value,
        dce: this.propertyFeatureOptions.filter(x => x.id === 'dce')[0].value,
        area: null
      },
      condominiumFeatures: {
        price: null,
        academy: this.condominiumFeatureOptions.filter(x => x.id === 'academy')[0].value,
        barbecue: this.condominiumFeatureOptions.filter(x => x.id === 'barbecue')[0].value,
        elevator: this.condominiumFeatureOptions.filter(x => x.id === 'elevator')[0].value,
        gourmetSpace: this.condominiumFeatureOptions.filter(x => x.id === 'gourmetSpace')[0].value,
        swimmmingPool: this.condominiumFeatureOptions.filter(x => x.id === 'swimmmingPool')[0].value,
        sauna: this.condominiumFeatureOptions.filter(x => x.id === 'sauna')[0].value,
        playground: this.condominiumFeatureOptions.filter(x => x.id === 'playground')[0].value,
        fullTimeReception: this.condominiumFeatureOptions.filter(x => x.id === 'fullTimeReception')[0].value,
        sportsCourt: this.condominiumFeatureOptions.filter(x => x.id === 'sportsCourt')[0].value,
        partyRoom: this.condominiumFeatureOptions.filter(x => x.id === 'partyRoom')[0].value,
        acceptsPet: this.condominiumFeatureOptions.filter(x => x.id === 'acceptsPet')[0].value
      }
    };
  }

  setFilter() {
    this.selectedRooms = this.filter.propertyFeatures.bedrooms;
    this.selectedBathrooms = this.filter.propertyFeatures.bathrooms;
    this.selectedParkingSpaces = this.filter.propertyFeatures.parkingSpaces;
    this.selectedPriceRange = this.filter.priceRange;
    this.selectedAreaRange = this.filter.areaRange;
    this.propertyFeatureOptions.map(x => {
      x.value = this.filter.propertyFeatures[x.id];
    });
    this.condominiumFeatureOptions.map(x => {
      x.value = this.filter.condominiumFeatures[x.id];
    });
  }

  reset(): void {
    this.selectedRooms = null;
    this.selectedBathrooms = null;
    this.selectedParkingSpaces = null;
    this.propertyFeatureOptions.map(x => x.value = false);
    this.condominiumFeatureOptions.map(x => x.value = false);
    this.selectedPriceRange = { floor: this.priceOptions.floor, ceil: this.priceOptions.ceil };
    this.selectedAreaRange = { floor: this.areaOptions.floor, ceil: this.areaOptions.ceil };
  }

}
