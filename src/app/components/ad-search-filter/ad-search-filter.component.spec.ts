import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdSearchFilterComponent } from './ad-search-filter.component';

describe('AdSearchFilterComponent', () => {
  let component: AdSearchFilterComponent;
  let fixture: ComponentFixture<AdSearchFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdSearchFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdSearchFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
