import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MatSelectChange, MatSelect, MatIconRegistry } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

export interface Filter {
  value: string;
  viewValue: string;
  toolTip: string;
  default: boolean;
}

@Component({
  selector: 'app-ad-card-toolbar',
  templateUrl: './ad-card-toolbar.component.html',
  styleUrls: ['./ad-card-toolbar.component.sass']
})
export class AdCardToolbarComponent implements OnInit {
  @Output() filterClick = new EventEmitter();
  @Output() selectionOptionFilter = new EventEmitter<string>();

  selectedOrder: string;

  optionFilters: Filter[] = [
    { value: '0', viewValue: 'Menor custo', toolTip: 'Ordena do mais barato para o mais caro', default: false },
    { value: '1', viewValue: 'Maior custo', toolTip: 'Ordena do mais caro para o mais barato', default: false },
    { value: '2', viewValue: 'Mais relevantes', toolTip: 'Ordena pelos anuncios mais relevantes', default: true },
    { value: '3', viewValue: 'Mais visitados', toolTip: 'Ordena pelos anuncios mais visitados', default: false }
  ];

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
      this.matIconRegistry.addSvgIcon("filter", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../assets/icons/filter.svg"))
                          .addSvgIcon("heart", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../assets/icons/heart.svg")) //TODO unificar SVG
    }

  ngOnInit() {
    this.selectedOrder = this.optionFilters.filter(x => x.default)[0].value;
  }

  onSelectionOptionFilter(event: MatSelectChange): void {
    this.selectionOptionFilter.emit(event.value);
  }

  onFilterClick(event: Event): void {
    event.stopPropagation();
    this.filterClick.next();
  }

}
