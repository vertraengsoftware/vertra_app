import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdCardToolbarComponent } from './ad-card-toolbar.component';

describe('AdCardToolbarComponent', () => {
  let component: AdCardToolbarComponent;
  let fixture: ComponentFixture<AdCardToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdCardToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdCardToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
