import { Component, OnInit, Input } from '@angular/core';
import { Gallery, GalleryItem, ImageItem } from '@ngx-gallery/core';

@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.component.html',
  styleUrls: ['./photo-gallery.component.sass']
})
export class PhotoGalleryComponent implements OnInit {
  @Input() photos: string[];
  @Input() groupLength: 1 | 2 | 3 | 4;
  photoGroups: string[][] = [];

  constructor(public gallery: Gallery) { }

  ngOnInit() {
    this.configGallery();
    this.fillPhotoGroups();
  }

  configGallery(): void {
    this.gallery.config.loadingMode = 'indeterminate';
    this.gallery.ref().load(
      this.photos.map(
        item => new ImageItem({ src: item, thumb: item })
      ));
  }

  fillPhotoGroups(): void {
    const quantityOfGroups = Math.ceil(this.photos.length / this.groupLength);
    const difference = this.photos.length % this.groupLength;
    let start = 0;

    for (let i = 0; i < quantityOfGroups; i++) {
      if ((i + 1) === quantityOfGroups && difference !== 0) {
        this.photoGroups.push(this.photos.slice(start, start + difference));
      } else {
        this.photoGroups.push(this.photos.slice(start, start + this.groupLength));
      }

      start += this.groupLength;
    }
  }

}
