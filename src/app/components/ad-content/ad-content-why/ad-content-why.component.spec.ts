import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdContentWhyComponent } from './ad-content-why.component';

describe('AdContentWhyComponent', () => {
  let component: AdContentWhyComponent;
  let fixture: ComponentFixture<AdContentWhyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdContentWhyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdContentWhyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
