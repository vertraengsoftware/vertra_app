import { Component, OnInit, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { Ad } from './../../../interfaces/ad/ad';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-ad-content-why',
  templateUrl: './ad-content-why.component.html',
  styleUrls: ['./ad-content-why.component.sass']
})
export class AdContentWhyComponent implements OnInit {

  @Input() adDetail: Ad;

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
      this.matIconRegistry.addSvgIcon("shakeHands", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/shakeHands.svg"))
      this.matIconRegistry.addSvgIcon("handPen", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/handPen.svg")) 
      this.matIconRegistry.addSvgIcon("cuttingMoney", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/cuttingMoney.svg"))
    }

  ngOnInit() {
  }

}
