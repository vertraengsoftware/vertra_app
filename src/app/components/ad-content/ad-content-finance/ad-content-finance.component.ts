import { Component, OnInit, Input } from '@angular/core';
import { Ad } from './../../../interfaces/ad/ad';
import { MatIconRegistry } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-ad-content-finance',
  templateUrl: './ad-content-finance.component.html',
  styleUrls: ['./ad-content-finance.component.sass']
})
export class AdContentFinanceComponent implements OnInit {

  @Input() adDetail: Ad;
  finance: number;

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
      this.matIconRegistry.addSvgIcon("creditCard", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/creditCard.svg"))
      this.matIconRegistry.addSvgIcon("documents", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/documents.svg")) 
    }

  ngOnInit() {
  }

  financeValue() {
    return Math.floor(this.adDetail.price.value/10);
  }
}
