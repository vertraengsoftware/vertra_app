import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdContentFinanceComponent } from './ad-content-finance.component';

describe('AdContentFinanceComponent', () => {
  let component: AdContentFinanceComponent;
  let fixture: ComponentFixture<AdContentFinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdContentFinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdContentFinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
