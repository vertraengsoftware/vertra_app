import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdContentFeaturesComponent } from './ad-content-features.component';

describe('AdContentFeaturesComponent', () => {
  let component: AdContentFeaturesComponent;
  let fixture: ComponentFixture<AdContentFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdContentFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdContentFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
