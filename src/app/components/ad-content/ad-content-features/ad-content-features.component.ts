import { Component, OnInit, Input } from '@angular/core';
import { Ad } from './../../../interfaces/ad/ad';
import { MatIconRegistry } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-ad-content-features',
  templateUrl: './ad-content-features.component.html',
  styleUrls: ['./ad-content-features.component.sass']
})
export class AdContentFeaturesComponent implements OnInit {

  @Input() adDetail: Ad;

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
      this.matIconRegistry.addSvgIcon("bedrooms", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/bedrooms.svg"))
                          .addSvgIcon("bathrooms", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/bathrooms.svg"))
                          .addSvgIcon("parkingSpaces", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/parkingSpaces.svg"))
                          .addSvgIcon("floor", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/floor.svg"))
                          .addSvgIcon("area", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/area.svg"))
                          .addSvgIcon("acceptsPet", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/acceptsPet.svg"))
                          .addSvgIcon("furnished", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/furnished.svg")) //TODO unificar SVG
    }

  ngOnInit() {
  }

}
