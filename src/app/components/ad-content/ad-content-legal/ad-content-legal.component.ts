import { Component, OnInit, Input } from '@angular/core';
import { Ad } from './../../../interfaces/ad/ad';
import { MatIconRegistry } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-ad-content-legal',
  templateUrl: './ad-content-legal.component.html',
  styleUrls: ['./ad-content-legal.component.sass']
})
export class AdContentLegalComponent implements OnInit {

  @Input() adDetail: Ad;

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
      this.matIconRegistry.addSvgIcon("legalStatus", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/legalStatus.svg")) }

  ngOnInit() {
  }

}
