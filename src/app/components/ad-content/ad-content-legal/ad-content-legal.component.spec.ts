import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdContentLegalComponent } from './ad-content-legal.component';

describe('AdContentLegalComponent', () => {
  let component: AdContentLegalComponent;
  let fixture: ComponentFixture<AdContentLegalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdContentLegalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdContentLegalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
