import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdContentAboutComponent } from './ad-content-about.component';

describe('AdContentAboutComponent', () => {
  let component: AdContentAboutComponent;
  let fixture: ComponentFixture<AdContentAboutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdContentAboutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdContentAboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
