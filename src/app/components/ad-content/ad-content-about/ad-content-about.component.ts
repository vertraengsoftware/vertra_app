import { Component, OnInit, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import { Ad } from './../../../interfaces/ad/ad';

@Component({
  selector: 'app-ad-content-about',
  templateUrl: './ad-content-about.component.html',
  styleUrls: ['./ad-content-about.component.sass']
})
export class AdContentAboutComponent implements OnInit {

  @Input() adDetail: Ad;
  randomText: number;
  neighborhoodDescription: string;

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
      this.matIconRegistry.addSvgIcon("about", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/about.svg"))
      this.matIconRegistry.addSvgIcon("floor", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/floor.svg"))
      this.matIconRegistry.addSvgIcon("condominium", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/condominium.svg")) }

  ngOnInit() {
    this.randomText = Math.floor(Math.random() * 1);
    this.neighborhoodDescription = "para quem procura o bairro para morar, sobram vantagens: vizinhança tranquila, segurança reforçada (boa parte pela presença da Polícia Militar no bairro), oferta crescente de serviços, qualidade de vida reforçada pela alta arborização e vasto leque de espaços de convívio, boa localização, facilidade de se deslocar para outros bairros, tanto em transporte público quanto particular… tudo, no bairro, é bastante convidativo ao morador que quer enxergar Belo Horizonte por novos olhos."
  }

}
