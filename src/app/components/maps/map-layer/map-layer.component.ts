import { Layer } from './../../../interfaces/map/layer';
import { Component, OnInit, Input } from '@angular/core';
import { MapMouseEvent } from 'mapbox-gl';

@Component({
  selector: 'app-map-layer',
  templateUrl: './map-layer.component.html',
  styleUrls: ['./map-layer.component.sass']
})
export class MapLayerComponent implements OnInit {
  @Input() hasPopup: boolean;
  @Input() layer: Layer;

  selectedPoint: GeoJSON.Feature<GeoJSON.Point> | null;

  constructor() { }

  ngOnInit() {
  }

  mouseMove(evt: MapMouseEvent): void {
    if (this.hasPopup) {
      document.body.style.cursor = 'default';
      this.selectedPoint = (evt as any).features[0];
    }
  }

  mouseLeave(): void {
    if (this.hasPopup) {
      this.selectedPoint = null;
    }
  }

}
