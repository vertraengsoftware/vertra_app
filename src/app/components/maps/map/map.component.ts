import { MapService } from './../../../services/map.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Layer } from 'src/app/interfaces/map/layer';
import { MatButtonToggleChange, MatIconRegistry } from '@angular/material';
import { Map, LngLatBounds } from 'mapbox-gl';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})
export class MapComponent implements OnInit {
  @Input() adsData: GeoJSON.FeatureCollection<GeoJSON.Point>;
  @Input() adSeeOnMap: GeoJSON.Feature<GeoJSON.Point>;
  @Input() showCityLayer: boolean;
  @Input() mapZoom: number[];
  @Input() mapCenter: number[];
  @Input() adLayerControls: boolean;
  @Output() mapMoveEnd = new EventEmitter<LngLatBounds>();

  map: Map;
  mapStyle = 'mapbox://styles/gtorresm/cjwr3q0x60ima1crwdg4t6h8g';
  layers: Layer[];
  cityBorder: Layer;
  neighborhood: Layer;

  constructor(private mapService: MapService, 
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
      this.matIconRegistry.addSvgIcon("valorizacao", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/valorizacao.svg")) 
                          .addSvgIcon("transito", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/transito.svg")) 
                          .addSvgIcon("educacao", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/educacao.svg"))
                          .addSvgIcon("saude", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/saude.svg"))
                          .addSvgIcon("restaurantes", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/restaurantes.svg")) 
                          .addSvgIcon("padariasesupermercados", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/padariasesupermercados.svg")) 
                          .addSvgIcon("vidaativa", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/vidaativa.svg"))
                          .addSvgIcon("inundacao", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../../assets/icons/inundacao.svg"))  //TODO unificar de acordo com layers.id
    }

  ngOnInit() {
    this.layers = this.mapService.getDefaultLayers();
    this.cityBorder = this.mapService.getCityBorder('belohorizonte');
    this.neighborhood = this.mapService.getCityBorder('bairros');
  }

  toggleLayer(evt: MatButtonToggleChange) {
    evt.value.layout = {
      ...evt.value.layout,
      visibility: evt.value.layout.visibility === 'visible' ? 'none' : 'visible'
    };
  }

  onMapMoveEnd(): void {
    const bounds = this.map.getBounds();
    this.mapMoveEnd.emit(bounds);
  }

  clearLayers(): void {
    this.layers.map(x => {
      x.layout = {
        ...x.layout,
        visibility: 'none'
      };
    });
  }

}
