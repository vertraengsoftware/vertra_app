import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapStorePopupComponent } from './map-store-popup.component';

describe('MapStorePopupComponent', () => {
  let component: MapStorePopupComponent;
  let fixture: ComponentFixture<MapStorePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapStorePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapStorePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
