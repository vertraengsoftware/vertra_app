import { Component, OnInit, Input } from '@angular/core';
import { Feature, Point } from 'geojson';

@Component({
  selector: 'app-map-store-popup',
  templateUrl: './map-store-popup.component.html',
  styleUrls: ['./map-store-popup.component.sass']
})
export class MapStorePopupComponent implements OnInit {
  @Input() feature: Feature<Point>;
  position: number[];

  constructor() { }

  ngOnInit() {
  }

}
