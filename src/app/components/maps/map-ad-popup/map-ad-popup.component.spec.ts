import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapAdPopupComponent } from './map-ad-popup.component';

describe('MapAdPopupComponent', () => {
  let component: MapAdPopupComponent;
  let fixture: ComponentFixture<MapAdPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapAdPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapAdPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
