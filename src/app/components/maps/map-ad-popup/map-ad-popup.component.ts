import { AdCard } from '../../../interfaces/ad/ad-card';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-map-ad-popup',
  templateUrl: './map-ad-popup.component.html',
  styleUrls: ['./map-ad-popup.component.sass']
})
export class MapAdPopupComponent implements OnInit {
  @Input() set selectedAd(val: GeoJSON.Feature<GeoJSON.Point>) {
    if (val) {
      this.showCard = true;
      if (!this.currentPoint || val.id !== this.currentPoint.id) {
        this.currentPoint = val;
        this.showCard = true;
        this.adCard = this.createAdCard(val.properties);
      }
    } else {
      this.showCard = false;
    }
  }

  currentPoint: GeoJSON.Feature<GeoJSON.Point>;
  showCard: boolean;
  adCard: AdCard;

  constructor() { }

  ngOnInit() {
  }

  createAdCard(properties: { [name: string]: any; }): AdCard {
    return {
      id: properties.id,
      photos: typeof properties.photos === 'string' ? JSON.parse(properties.photos) : properties.photos,
      price: typeof properties.price === 'string' ? JSON.parse(properties.price) : properties.price,
      property: typeof properties.property === 'string' ? JSON.parse(properties.property) : properties.property
    };
  }

}
