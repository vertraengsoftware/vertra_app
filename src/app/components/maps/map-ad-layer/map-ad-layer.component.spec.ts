import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapAdLayerComponent } from './map-ad-layer.component';

describe('MapAdLayerComponent', () => {
  let component: MapAdLayerComponent;
  let fixture: ComponentFixture<MapAdLayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapAdLayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapAdLayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
