import { Component, OnInit, Input } from '@angular/core';
import { MapMouseEvent } from 'mapbox-gl';
import { Router } from '@angular/router';

@Component({
  selector: 'app-map-ad-layer',
  templateUrl: './map-ad-layer.component.html',
  styleUrls: ['./map-ad-layer.component.sass']
})
export class MapAdLayerComponent implements OnInit {
  @Input() data: GeoJSON.FeatureCollection<GeoJSON.Point>;
  @Input() hasControls: boolean;

  @Input() set adSeeOnMap(val: GeoJSON.Feature<GeoJSON.Point>) {
    this.selectedAd = val;
  }

  selectedAd: GeoJSON.Feature<GeoJSON.Point> | null;

  public adLayer: any = {
    id: 'ads',
    label: 'Anúncios',
    type: 'circle',
    style: {
      'circle-radius': 4,
      'circle-color': '#5E35F2',
      'circle-stroke-width': 3,
      'circle-stroke-color': '#FFFFFF'
    }
  };

  constructor(private router: Router) { }

  ngOnInit() {
  }

  mouseMove(evt: MapMouseEvent): void {
    if (this.hasControls) {
      this.selectedAd = (evt as any).features[0];
    }
  }

  mouseLeave(): void {
    if (this.hasControls) {
      this.selectedAd = null;
    }
  }

  click(evt: MapMouseEvent): void {
    if (this.hasControls) {
      this.selectedAd = (evt as any).features[0];
      this.router.navigate(['/anuncio', this.selectedAd.id]);
    }
  }

}
