import { AdCard } from './../../interfaces/ad/ad-card';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-ad-card',
  templateUrl: './ad-card.component.html',
  styleUrls: ['./ad-card.component.sass']
})
export class AdCardComponent implements OnInit {
  @Input() adCard: AdCard;
  @Input() showCarouselControls: boolean;
  @Input() showBtnSeeMap: boolean;
  @Output() seeMapClick = new EventEmitter();

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {
      this.matIconRegistry.addSvgIcon("bedrooms", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../assets/icons/bedrooms.svg"))
                          .addSvgIcon("bathrooms", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../assets/icons/bathrooms.svg"))
                          .addSvgIcon("area", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../assets/icons/area.svg")) //TODO unificar SVG
    }

  ngOnInit() {
  }

  onSeeOnMap(event: Event): void {
    event.stopPropagation();
    this.seeMapClick.next();
  }

  Value() {
    return Math.floor(this.adCard.price.value);
  }  

}
