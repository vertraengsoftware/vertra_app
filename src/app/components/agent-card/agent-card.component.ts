import { Component, OnInit,Input, AfterViewInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import { Ad } from './../../interfaces/ad/ad';

@Component({
  selector: 'app-agent-card',
  templateUrl: './agent-card.component.html',
  styleUrls: ['./agent-card.component.sass']
})
export class AgentCardComponent implements OnInit {

@Input() adDetail: Ad;
  
@ViewChild('stickyMenu', {static: false}) menuElement: ElementRef;

sticky: boolean = false;

constructor(private matIconRegistry: MatIconRegistry,
  private domSanitizer: DomSanitizer) { 
    this.matIconRegistry.addSvgIcon("legalStatus", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../assets/icons/legalStatus.svg"))
    this.matIconRegistry.addSvgIcon("vertraLogo", this.domSanitizer.bypassSecurityTrustResourceUrl("./../../../assets/logos/vertraLogo.svg"))
  }

ngOnInit() {
}

@HostListener('window:scroll', ['$event'])
  handleScroll(){
    const windowScroll = window.pageYOffset;
    if(windowScroll >= 500){
      this.sticky = true;
    } else {
      this.sticky = false;
    }
  }

  Value() {
    return Math.floor(this.adDetail.price.value);
  }

  ValueIptu() {
    return Math.floor(this.adDetail.property.iptu.value);
  }

  ValueCondominio() {
    return Math.floor(this.adDetail.property.condominiumFeatures.price.value);
  }

}
