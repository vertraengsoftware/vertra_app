import { Component, OnInit } from '@angular/core';
import { AdSearchService } from 'src/app/services/ad-search.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-ad-search',
  templateUrl: './ad-search.component.html',
  styleUrls: ['./ad-search.component.sass']
})
export class AdSearchComponent implements OnInit {
  word = '';
  constructor(private adService: AdSearchService, private router: Router) { }

  ngOnInit() {
  }

  search() {
    //this.adService.searchAd(this.word); //TODO ver uncionamento
    this.router.navigateByUrl('/busca'); //TODO always /busca
  }


}
