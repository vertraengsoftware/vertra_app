export interface Checkbox {
    id: string;
    label: string;
    value: boolean;
}
