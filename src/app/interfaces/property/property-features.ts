export interface PropertyFeatures {
    bedrooms: number;
    suites: number;
    bathrooms: number;
    parkingSpaces: number;
    floor: number;
    pentHouse: boolean; // cobertura
    furnished: boolean; // mobiliado
    privateSwimmingPool: boolean;
    dce: boolean;
    area: number;
}
