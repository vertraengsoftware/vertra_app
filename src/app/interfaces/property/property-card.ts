import { PropertyFeatures } from './property-features';
import { Coordinates } from '../general/coordinates';

export interface PropertyCard {
    id: string;
    type: string;
    street: string;
    neighborhood: string;
    city: string;
    coordinates: Coordinates;
    features: PropertyFeatures;
}
