import { Price } from './../general/price';

export interface CondominiumFeatures {
    price: Price;
    academy: boolean;
    barbecue: boolean;
    elevator: boolean;
    gourmetSpace: boolean;
    swimmmingPool: boolean;
    playground: boolean;
    fullTimeReception: boolean;
    sportsCourt: boolean;
    partyRoom: boolean;
    sauna: boolean;
    acceptsPet: boolean;
}
