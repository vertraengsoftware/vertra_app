import { PropertyType } from './property-type.enum';
import { Address } from '../general/address';
import { PropertyFeatures } from './property-features';
import { CondominiumFeatures } from './condominium-features';
import { Price } from '../general/price';
import { Coordinates } from '../general/coordinates';

export interface Property {
    id: string;
    type: PropertyType;
    description: string;
    photos: string[];
    address: Address;
    coordinates: Coordinates;
    features: PropertyFeatures;
    condominiumFeatures: CondominiumFeatures;
    iptu: Price;
}
