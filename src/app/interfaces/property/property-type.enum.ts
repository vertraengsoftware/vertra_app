export enum PropertyType {
    apartment = 1,
    house = 2,
    condominiumHouse = 3,
    studio = 4
}
