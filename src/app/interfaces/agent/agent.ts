import { Address } from '../general/address';
import {AgentType} from './agent-type'
import {AgentContact} from './agent-contact'

export interface Agent {
    id: string; // cpf / cnpj
    creci: number;
    type: AgentType;
    name: string;
    contact: AgentContact;
    address:  Address;
    description: string;
    photos: string[];
    registerDate: Date;
    indication: number;
    coverPhoto: string;
}