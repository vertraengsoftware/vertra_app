export interface AgentContact {
    fone: number;
    ddd: number;
    email: string;
    wpp: number;
    site: string;
}