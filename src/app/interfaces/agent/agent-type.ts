export enum AgentType {
    imobiliaria = 1,
    autonomo = 2,
    parceiro = 3
}