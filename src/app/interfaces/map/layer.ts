export interface Layer {
    id: string;
    label: string;
    url: string;
    sourceLayer: string;
    type: 'symbol' | 'fill' | 'line' | 'circle' | 'fill-extrusion' | 'raster' | 'background';
    style: any;
    layout: any;
}
