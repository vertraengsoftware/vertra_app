export interface Range {
    floor: number;
    ceil: number;
}
