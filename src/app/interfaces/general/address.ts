import { KeyValue } from './key-value';

export interface Address {
    street: string;
    number: string;
    complement: string;
    neighborhood: string;
    zipcode: string;
    city: KeyValue<string, string>;
    state: KeyValue<string, string>;
    country: KeyValue<string, string>;
}
