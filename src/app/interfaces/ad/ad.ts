import { AdStatus } from './ad-status.enum';
import { AdType } from './ad-type.enum';
import { Price } from '../general/price';
import { Property } from '../property/property';
import {Agent} from '../agent/agent'

export interface Ad {
    id: string;
    type: AdType;
    status: AdStatus;
    price: Price;
    property: Property;
    registerDate: Date;
    agents: Agent[];
}
