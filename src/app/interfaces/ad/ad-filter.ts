import { AdType } from './ad-type.enum';
import { Range } from '../general/range';
import { PropertyType } from '../property/property-type.enum';
import { CondominiumFeatures } from '../property/condominium-features';
import { PropertyFeatures } from '../property/property-features';
import { Address } from '../general/address';

export interface AdFilter {
    adType: AdType;
    priceRange: Range;
    propertyType: PropertyType;
    areaRange: Range;
    address: Address;
    propertyFeatures: PropertyFeatures;
    condominiumFeatures: CondominiumFeatures;
}
