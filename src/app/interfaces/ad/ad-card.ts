import { PropertyCard } from './../property/property-card';
import { Price } from '../general/price';

export interface AdCard {
    id: string;
    price: Price;
    property: PropertyCard;
    photos: string[];
}
