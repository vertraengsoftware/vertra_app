export enum AdContactType {
    whatsApp = 1,
    phone = 2,
    email = 3
}
