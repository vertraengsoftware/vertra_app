export enum AdStatus {
    pending = 1,
    active = 2,
    sold = 3,
    rented = 4,
    inactive = 5,
    refused = 6
}
