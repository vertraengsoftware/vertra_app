import { AdFilter } from './../../interfaces/ad/ad-filter';
import { LngLatBounds } from 'mapbox-gl';
import { AdCard } from './../../interfaces/ad/ad-card';
import { AdSearchService } from './../../services/ad-search.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-container',
  templateUrl: './search-container.component.html',
  styleUrls: ['./search-container.component.sass']
})
export class SearchContainerComponent implements OnInit {
  adsData: GeoJSON.FeatureCollection<GeoJSON.Point>;
  adSeeOnMap: GeoJSON.Feature<GeoJSON.Point>;
  filterIsOpen: boolean;
  mapZoom = [12];
  mapCenter = [-43.958311, -19.911375];
  filter: AdFilter;

  constructor(public adSearchService: AdSearchService) { }

  ngOnInit() {
    this.firstAds();

    this.mapAds();

    this.adSearchService.adCardsMap$.subscribe(data => {
      this.adsData = data ? this.adSearchService.adCard2GeoJsonFeatureCollection(data) : null;
    });

    this.adSearchService.adFilter$.subscribe(x => {
      this.filter = x;
    });

    this.filterIsOpen = false;
  }

  firstAds(): void {
    this.adSearchService.getFirstAds();
  }

  mapAds(): void {
    this.adSearchService.getMapAds();
  }

  showFilter(): void {
    this.filterIsOpen = true;
  }

  closeFilter(): void {
    this.filterIsOpen = false;
  }

  resetFilter(): void {
    this.adSearchService.filter(null);
  }

  applyFilter(filter: AdFilter): void {
    this.adSearchService.filter(filter);
    this.adSearchService.filterMap(filter);
    this.filterIsOpen = false;
  }

  openAdCardOnMap(selectedAdCard: AdCard): void {
    console.log('teste');

    if (selectedAdCard) {
      this.adSeeOnMap = this.adSearchService.adCard2GeoJsonFeature(selectedAdCard);
    }
  }

  onScrolled(): void {
    this.adSearchService.getNextPage(this.adsData.features.length);
  }

  mapMoveEnd(bounds: LngLatBounds): void {
    const north = bounds.getNorth();
    const south = bounds.getSouth();
    const east = bounds.getEast();
    const west = bounds.getWest();

    this.adSearchService.filterByBounds(north, south, east, west);
  }

}
