import { AdSearchService } from 'src/app/services/ad-search.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page-container',
  templateUrl: './landing-page-container.component.html',
  styleUrls: ['./landing-page-container.component.sass']
})
export class LandingPageContainerComponent implements OnInit {
  word = '';
  constructor(private adService: AdSearchService, private router: Router) { }

  ngOnInit() {
  }

  search() {
    this.router.navigateByUrl('/busca'); //TODO always /busca
  }
}
