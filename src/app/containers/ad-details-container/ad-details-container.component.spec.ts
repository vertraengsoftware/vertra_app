import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdDetailsContainerComponent } from './ad-details-container.component';

describe('AdDetailsContainerComponent', () => {
  let component: AdDetailsContainerComponent;
  let fixture: ComponentFixture<AdDetailsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdDetailsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdDetailsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
