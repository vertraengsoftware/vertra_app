import { BehaviorSubject } from 'rxjs';
import { AdService } from './../../services/ad.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ad } from 'src/app/interfaces/ad/ad';
import { AdContactType } from 'src/app/interfaces/ad/ad-contact-type.enum';

export enum HeaderContentType {
  general,
  photos,
  map,
  streetView
}

@Component({
  selector: 'app-ad-details-container',
  templateUrl: './ad-details-container.component.html',
  styleUrls: ['./ad-details-container.component.sass']
})
export class AdDetailsContainerComponent implements OnInit {
  adData: GeoJSON.FeatureCollection<GeoJSON.Point>;
  currentAd: Ad;
  mapCenter: number[];
  headerContentType: HeaderContentType;
  btnChecked: 'General';

  constructor(public adService: AdService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getAdDetails();

    this.adService.currentAd$.subscribe(x => {
      if (x) {
        this.currentAd = x;
        this.getGeoJson();
      }
    });

    this.changeContentType(HeaderContentType.general);
  }

  getAdDetails(): void {
    this.adService.getDetails(this.route.snapshot.paramMap.get('id'));
  }

  getGeoJson(): void {
    this.adData = this.adService.ad2GeoJsonFeatureCollection(this.currentAd);
    this.mapCenter = this.adData.features[0].geometry.coordinates;
  }

  changeContentType(type: HeaderContentType): void {
    this.headerContentType = type;
  }

  contactClick(obj: { id: string, type: AdContactType }): void {
    this.adService.saveContact(obj.id, obj.type);
  }

}
