# Visão geral
O principal objetivo do nosso MVP é entregar ao nosso usuário informações que podem influenciar diretamente na busca por imóveis de uma forma simples e amigável. Diante disso, selecionamos algumas funcionalidades que nos ajudarão a cumprir essa proposta.

# Funcionalidades
- Busca por imóveis através de filtros padrões e mapa
- Visualização das informações dos imóveis e respectivas fotos
- Demonstração de interesse no anúncio através de contato via e-mail, WhatsApp ou telefone

# Visão técnica
Visando estarmos aptos para expandirmos para outros canais no futuro, nossa aplicação é separada em duas. A primeira delas é o front-end (vertra-app) desenvolvido em Angular (versão 8). O back-end (vertra-api) está sendo desenvolvido em .NET Core. Toda a inteligência da aplicação deve ficar na API, possibilitando assim a reutilização da mesma no futuro, em um aplicativo mobile por exemplo.


##Front-end (vertra-app)
Estamos utilizando a versão 8 do Angular e seguindo as boas práticas da documentação oficial. Portanto todas as nossas telas são divididas em componentes, visando sua reutilização. Além disso estamos utilizando um padrão de componentes conhecido como "containers". O motivo é termos separados os componentes inteligentes (com lógica de negócio e acoplados a services) dos componentes burros (os que apenas recebem informações e as exibem). [Aqui](https://blog.angularindepth.com/container-components-with-angular-11e4200f8df) temos um artigo que explica bem esse padrão.
###Diretórios
Visando uma melhor organização do código, temos alguns diretórios para separarmos os arquivos. Seguem abaixo os principais:
- Components (componentes burros)
- Containers (componentes inteligentes)
- Interfaces (classes)
- Services (lógicas de negócio e integrações com API)
- Assets (imagens e arquivos)
###Dados mockados
Atualmente não finalizamos o desenvolvimento da nossa API e nem da base de dados. Portanto, todos os dados que estão sendo exibidos no nosso site são mockados em um arquivo JSON. Usamos uma ferramenta que serve esse arquivo JSON como se fosse um banco de dados e expõe uma API. Dessa forma, conseguimos implementar todas as chamadas nos services da nossa aplicação Angular normalmente. Portanto assim que o back-end estiver pronto, bastará trocar as chamadas. [Aqui](https://github.com/typicode/json-server) temos a documentação completa do JSON Server que realiza o trabalho de servir o arquivo JSON como base de dados e de expor essa base de dados como uma API.
###Publicação no Firebase
Para publicar a aplicação no Firebase basta seguir esse [link](https://www.positronx.io/deploy-angular-8-app-to-firebase/). A aplicação sempre foi publicada seguindo esses passos.
Como citado acima, usamos um arquivo JSON que é servido como base de dados. Portanto, precisamos publicar esse arquivo também, sempre que algo for alterado. Existe uma [ferramenta](https://github.com/narze/firebase-json-server) que nos auxilia nessa publicação, também no firebase.

### Arquitetura
Está sendo implementada uma arquitetura usando como base o [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html). Portanto, a ideia é que sejam aplicados conceitos como [Injeção de Dependência](https://www.freecodecamp.org/news/a-quick-intro-to-dependency-injection-what-it-is-and-when-to-use-it-7578c84fa88f/), [Repository](https://deviq.com/repository-pattern/), [SOLID](https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design). Recomendo que o back-end seja todo desenvolvido antes de pensarem no banco de dados. Visando isso, existe um projeto (Vertra.Repository.Mock) que é destinado para conter as implementações dos repositórios que retornam os dados [mock](https://www.codeproject.com/Articles/47603/Mock-a-Database-Repository-using-Moq). Toda a lógica da aplicação deve ficar isolada no projeto Vertra.Core. Esse projeto não deve depender de ninguém, apenas os outros projetos que podem depender dele (princípio da inversão de dependência). 

## Mapas
Desenvolvemos duas ferramentas para nos auxiliar na elaboração dos mapas. Os fontes de ambas estão no repositório. A primeira delas é uma aplicação que realiza uma busca no [Yelp](https://www.yelp.com/) e retorna um GeoJSON já pronto para ser importado no [MapBox](https://www.mapbox.com/). A segunda delas é uma ferramenta feita em NodeJS que recebe como entrada um GeoJSON com as coordenadas em um padrão diferente e retorna esses mesmo pontos na nossa coordenada. Além disso, essa última ferramenta também consegue realizar filtros em arquivos GeoJSON.
Na aplicação angular usamos uma biblioteca chamada [ngx-mapbox-gl](https://github.com/Wykks/ngx-mapbox-gl) que realiza a comunicação com o MapBox. Além disso, toda essa parte já está implementada no código.

## Padrão de desenvolvimento
Estamos utilizando nos repositórios o padrão do [gitflow](https://github.com/nvie/gitflow). Portanto nos repositórios do front e do back-end não terá nada nas branches master, apenas nas de desenvolvimento e de features.
