# VertraApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Gerenciamento ágil

- Planilha de apuuração de horas
  https://docs.google.com/spreadsheets/d/113darBlVcVaNmUcxCv6jOCoZyuB8_0nI1qcRxVvcIdU/edit?usp=sharing
- Gráfico burndown
  https://docs.google.com/document/d/1KYf3qca96qb6Xm__f_3KOdN0qsuGHOlSXkFW9pr-jHA/edit?usp=sharing

## Develop using Docker and Docker-compose

- Install docker:

  Go to https://docs.docker.com/install/, choose your preferred platform and follow the instructions.

- Install docker-compose

  Go to https://docs.docker.com/compose/install/, choose your preferred platform and follow the instructions.

- Development Server:

  Run `docker-compose up --build -d` to build and start a docker container with the development environment. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. The json-server API will be accessible on `http://localhost:3000/`.

- Running commands inside container:

  With the docker-compose environment running, use the command line:
  
  ```
  docker-compose exec angular <command>
  ```

  **Examples:**
  
  ```
  docker-compose exec angular npm install @angular/cli --save # Install new NPM library
  docker-compose exec angular ng build # Build the angular project
  docker-compose exec angular sh # Get a shell inside the container
  ```

- Container logs:

  To watch container logs like with `tail -f`  run `docker-compose logs -f`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
