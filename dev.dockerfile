FROM node:10.16-alpine

EXPOSE 4200

WORKDIR /app

COPY package.json /app/
COPY package-lock.json /app/

RUN npm -g install @angular/cli \
    && npm install

CMD [ "ng", "serve", "--host", "0.0.0.0", "--disableHostCheck" ]